<?php
/**
 * File path: /resources/blocks/blockName.php
 */

/**
 * Register the block
 * @link https://www.advancedcustomfields.com/resources/acf_register_block/ for more details
 */
acf_register_block(array(
    'name'              => 'blockName',
    'title'             => __('Block Name'),
    'description'       => __('Description'),
    'render_callback'   => 'block_name_renderer',
    'category'          => 'theme-name-blocks',
    'icon'              => 'star-filled',
    'keywords'          => array('keywords', 'theme-name'),
    'mode'              => 'edit',
));

/**
 * Method that renders block on the front end. Do all your logic here
 */
function block_name_renderer($block) {
    global $post; // current post
    $slug = str_replace('acf/', '', $block['name']);

    $data = [
        
    ];

    // Pass all data to the view
    echo \App\sage('blade')->render('blocks.' . $slug, $data);
}
