##Setup##

1. Before you do anything, make sure the entire contents of this repository are in the same folder (the README is not required, but it helps for the next dev who stumbles across this).
2. Update the constants in sageBlock.command to reflect your work environment:

	BASE should be the path from ~(home) to the folder that this repository lives in. 
	
	DEST should be the path from ~(home) to the destination folder, the only thing that should be updated for this constant is the prefix before the $folderName to reflect your valet setup.
	
3. If you want the newly generated block style file to be automatically enqueued in your main.scss file, add a /* SHELL to the line where you want the new files to be added. I recommend adding a " - DO NOT REMOVE, RELATED TO sageBlock.command" to this line to discourage other devs from removing it.

##Run Instructions##

1. Double click sageBlock.command in Finder.
2. Provide block details in the terminal window that opens.
3. ???? - the script will create a Controller file, a View file, and a SCSS file in the background based on the details you provide.
4. Profit (or go and update the remaining fields in the Controller file, and hook up the new SCSS file to the main.scss if you haven't set up automatic enqueueing)