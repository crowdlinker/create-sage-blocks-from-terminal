#!/bin/bash

## Get User Input
read -p "Parent Folder Name: "  folderName
read -p "Theme Name: "  themeName
read -p "Proper Block Name: "  blockName
read -p "Block Description: "  blockDesc

## Assign Constants
BASE="/work/BLOCK_BASE"
DEST="/work/$folderName/wp-content/themes/$themeName/resources"

## Assign Variables
lowerBlockName=$( echo "$blockName" | tr '[:upper:]' '[:lower:]' | tr ' ' '-')
lowerBlockNameUnderscore=$( echo "$lowerBlockName" | tr '-' '_')
# File Variables
regFile="$HOME/$DEST/blocks/$lowerBlockName.php"
viewFile="$HOME/$DEST/views/blocks/$lowerBlockName.blade.php"
sassFile="$HOME/$DEST/assets/styles/blocks/_$lowerBlockName.scss"
mainSassFile="$HOME/$DEST/assets/styles/main.scss"

## Start Main Block Creation
echo "Creating new block '$blockName' in theme '$themeName'..."

# Go to base folder
cd $HOME/$BASE

# Create the block registration file
echo "Creating block registration file..."
cp block.php $regFile
sed -i '' "s/Block Name/$blockName/g" $regFile
sed -i '' "s/blockName/$lowerBlockName/g" $regFile
sed -i '' "s/block_name/$lowerBlockNameUnderscore/g" $regFile
sed -i '' "s/Description/$blockDesc/g" $regFile
sed -i '' "s/theme-name/$themeName/g" $regFile

# Create the block view file
echo "Creating block view file..."
cp block.blade.php $viewFile
sed -i '' "s/blockName/$lowerBlockName/g" $viewFile

# Create the block style file
echo "Creating block SCSS file..."
cp _block.scss $sassFile
sed -i '' "s/blockName/$lowerBlockName/g" $sassFile

# Queue the style file in the main.scss file and add a new line before the insert shiv
sed -i '' "s/\/\* SHELL/@import \"blocks\/$lowerBlockName\";\/\* SHELL/" $mainSassFile
sed -i '' 's/\/\* SHELL/\'$'\n\/\* SHELL/' $mainSassFile

